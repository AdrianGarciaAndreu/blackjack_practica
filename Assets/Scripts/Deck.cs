﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class Deck : MonoBehaviour
{
    public Sprite[] faces;
    public GameObject dealer;
    public GameObject player;
    public Button hitButton;
    public Button stickButton;
    public Button playAgainButton;
    public Text finalMessage;
    public Text probMessage;

    public int[] values = new int[52];
    int cardIndex = 0;

    //Cartas con valor y cara
    [HideInInspector]
    [System.Serializable]
    public struct Card
    {
        public Sprite face;
        public int index;
        public int value;
    };

    //Listado de cartas barajadas para el juego
    
    public List<Card> ShuffledCards;
    public List<Card> UnshuffledCards;


    private void Awake()
    {    
        InitCardValues();        

    }

    private void Start()
    {
        ShuffleCards();
        StartGame();        
    }

    private void InitCardValues()
    {

        int deckCounter = 0;
        int typesCounter = -1;
        foreach (Sprite card in faces) {

            //Valores noralizados entre 0 y 12 para los tipos de cartas
            if (typesCounter + 1 > 12) { typesCounter = 0; }
            else { typesCounter++; }

            //Establece valores iniciales para las cartas en función de la cara de las cartas
            values[deckCounter] = SetCardValue(typesCounter);
            deckCounter++;


        }

    }


    /*
     * En función del valor de la cara de una carta.
     * Retorna el valor correspondiente para el blackJack.
     */
    private int SetCardValue(int faceTypeIndex)
    {
        int value = 0;
        switch (faceTypeIndex)
        {
            case 0: //Valor para ases
                value = 11;
            break;

            case 10: case 11: case 12: //Valor para figuras
                value = 10;
            break;

            default: //Valores para los números entre 1 y 10
                value = faceTypeIndex + 1;
            break;

        }

        return value;
    }


    /*
     * Se barajan las cartas de forma aleatoria
     */
    private void ShuffleCards()
    {

        this.ShuffledCards = new List<Card>();
        this.UnshuffledCards = new List<Card>();

        //Copia el listado de cartas a una lista común con las cartas y sus valores
        for (int i = 0; i < faces.Length; i++) {
            Card c = new Card();
            c.index = i; c.face = this.faces[i]; c.value = this.values[i];
            this.UnshuffledCards.Add(c);
        }

        //Se introducen en otro listado de forma aleatoria
        while (this.UnshuffledCards.Count > 0){
            int c_index = Random.Range(0, this.UnshuffledCards.Count);
            this.ShuffledCards.Add(this.UnshuffledCards[c_index]);
            this.UnshuffledCards.RemoveAt(c_index);
            
        }


    }


    

    void StartGame()
    {
        for (int i = 0; i < 2; i++)
        {
            PushPlayer();
            PushDealer();
            
            int p_player = PlayerPoints(this.player); int p_dealer = PlayerPoints(this.dealer);
            if(p_player>=21 || p_dealer >= 21){ WhoWins(); }
        }
    }


    public double Findfactorial(int number){
        double fact = 1;
        for (int i = 1; i <= number; i++){
            fact = fact * i;
        }

        return fact;
    }



    private void CalculateProbabilities()
    {

        ///Resultados 
        float p_pasarse = 0;
        float p_empatar = 0;
        float p_pasarse_dealer = 0;


        float n_aces = 1; // >10 || 1
        float n_figures = 4; // == 10
        float n_posibles = 13; //Posibles cartas a salir




        //Probabilidad de pasarse
        int p_actuales = PlayerPoints(this.player); //Puntos actuales
        int p_numCartas = this.player.GetComponent<CardHand>().cards.Count; //Cantidad de cartas que tiene el jugador

        int p_toMore21 = 22 - p_actuales; //Diferencia para ganar
        if (p_toMore21 > 0)
        {
            if (p_toMore21 == 10) //Si se la diferencia es de 10, solo se puede pasar con los ases
            {
                p_pasarse = Mathf.Min( (Mathf.Round((n_figures + n_aces) / n_posibles * 100) * 100f)/100f,100);
            } else if (p_toMore21 <= 9) //Si es inferior, los casos probables se incrementan de 1 en 1, en función de la diferencia respecto a la proababilidad de solo pasarnos con un as
            {
                int c_favorables = 10 - p_toMore21;
                p_pasarse = Mathf.Min(Mathf.Round((((n_figures + n_aces) + c_favorables) / n_posibles * 100)*100f)/100f,100);
            }
        } else { p_pasarse = 100; } //Si hay más de 21, la probabilidad es 100%


        //Probabilidad de entre 17 y 21
        int p_empatar_rango = 0;
        int p_to21 = 21 - p_actuales; //Diferencia para ganar
        for (int i= p_to21; i>=(p_to21-4); i--){
            p_empatar_rango++;
            float p_empatar_sin_acumular = 0;

                if (i == 10) {
                    p_empatar_sin_acumular = (n_figures + n_aces) / n_posibles;
                }
                else if (i <= 9){
                    int c_favorables = 10 - i;
                    p_empatar_sin_acumular = ((n_figures + n_aces) + c_favorables) / n_posibles;
                }

            p_empatar += p_empatar_sin_acumular;
        }

        p_empatar = Mathf.Min(Mathf.Round(((p_empatar / p_empatar_rango)*100)*100f)/100f,100);

        

        //Probabilidad de que el crupier tenga más puntos que el jugador
        if (this.dealer.GetComponent<CardHand>().cards.Count > 1) { 
            float d_conocidos = this.dealer.GetComponent<CardHand>().cards[1].GetComponent<CardModel>().value;
            float d_numCartas = this.dealer.GetComponent<CardHand>().cards.Count;           

            if (p_actuales > d_conocidos) {
                float d_masQue_p = ((p_actuales + 1) - d_conocidos);
                

                //Puntos necesitados entre las cartas ocultas que hay para superar al jugador
                float d_necesitados = Mathf.Round((d_masQue_p) / (d_numCartas - 1)); 
                float d_favorables = 0;

                if (d_necesitados <= 11) {
                    if (d_necesitados == 14) { d_favorables = (n_aces); }
                    if (d_necesitados == 10) { d_favorables = (n_figures + n_aces); }
                    else if (d_necesitados < 9) {
                        d_favorables = (10 - d_necesitados) + ((n_figures + n_aces)); 
                    }


                    if (d_favorables > 0) { 
                        double CR_favorables = Findfactorial((int)(d_favorables + (d_numCartas -1) -1)) / (Findfactorial((int)(d_favorables - (d_numCartas-1))) - Findfactorial((int)(d_numCartas-1)));
                        double CR_posibles = Findfactorial((int)(13 + (d_numCartas - 1) - 1)) / (Findfactorial((int)(13 - (d_numCartas - 1))) - Findfactorial((int)(d_numCartas - 1)));
                                

                        float d_laplace = (float)(CR_favorables / CR_posibles);
                        p_pasarse_dealer = Mathf.Round((d_laplace * 100)*100f)/100f;
                    }
                }
                else { p_pasarse_dealer = 0; }
            }
            else { p_pasarse_dealer = 100; }
        }
        
        //Se muestra el mensaje
        probMessage.text = 
            "Prob. \ncrupier más que jugador: \n"+ p_pasarse_dealer + "% \n\n" 
            + "Prob. entre 17 y 21: \n"+p_empatar+"%\n\n" 
            +"Prob. de pasarse de 21: \n" + p_pasarse+"%";

    }



    private void coverCard(GameObject card){
        CardModel cm = card.GetComponent<CardModel>();
        cm.ToggleFace(false);
    }


    private void DiscoverCards() {
         List<GameObject> cards = this.dealer.GetComponent<CardHand>().cards;
        foreach(GameObject card in cards){
            CardModel cm = card.GetComponent<CardModel>();
            cm.ToggleFace(true);
        }
    }



    void PushDealer()
    {

        GameObject card = dealer.GetComponent<CardHand>().Push(this.ShuffledCards[cardIndex].face, this.ShuffledCards[cardIndex].value);
        if (this.dealer.GetComponent<CardHand>().cards.Count > 2){
            coverCard(card);
        }

        cardIndex++;
        CalculateProbabilities();
    }



    /*
     * Función para que si la siguiente carta es un As, y se pasa de la puntuación máxima.
     * Este As valga 1 de forma automática
     */
    private void NextIsAce(GameObject player) {
        
        //Se comprueba la puntuación actual, para autoasignar el valor a los ases
        int bj = 0;
        CardHand playerCards = player.GetComponent<CardHand>();
        foreach (GameObject card in playerCards.cards){
            int _value = card.GetComponent<CardModel>().value;
            bj += _value;
        } //Puntuación actual del jugador

        //Si la carta a coger es un As, y al valer 11 se superan los 21, está se interpreta como de valor 1
        bj += this.ShuffledCards[cardIndex].value;
        if (this.ShuffledCards[cardIndex].value == 11 && bj >21) {

            Card c = this.ShuffledCards[cardIndex]; c.value = 1;
            this.ShuffledCards[cardIndex] = c;

            this.values[c.index] = 1;
        }
        

    }


    void PushPlayer()
    {
        NextIsAce(this.player);
        player.GetComponent<CardHand>().Push(this.ShuffledCards[cardIndex].face, this.ShuffledCards[cardIndex].value);
        cardIndex++;
        CalculateProbabilities();
    }       

    public void Hit()
    {
        
        //Repartimos carta al jugador
        PushPlayer();

        if (PlayerPoints(this.player)>21) {
            DiscoverCards();
            finalMessage.text = "Has periddo. Victoria para la banca.";
            hitButton.interactable = false;
            stickButton.interactable = false;
        }

    }


    /*
     * Checks if a player curent points
     */
    private int PlayerPoints(GameObject go) {
        
        int bj = 0;

        CardHand playerCards = go.GetComponent<CardHand>();
        foreach (GameObject card in playerCards.cards) {
            int _value = card.GetComponent<CardModel>().value;
            bj += _value; 
        }

        return bj;
    }


    public void Stand()
    {

        if (PlayerPoints(this.dealer)<17){
            PushDealer();
        } else { WhoWins(); }


    }

    private void WhoWins() {

        //Se descubren las cartas de la banca
        DiscoverCards();

        int p_player = PlayerPoints(this.player);
        int p_dealer = PlayerPoints(this.dealer);

        //Diferencias hasta la puntuación perfecta
        int p_diff = Mathf.Abs(21 - p_player);
        int d_diff = Mathf.Abs(21 - p_dealer);

        //Si la diferencia del jugador es menor respecto a 21 o igual que la del dealer, se considera una victoria (suponiendo que el dealer no se pase de 21)
        if ((p_diff <= d_diff) || (p_player<=21 && p_dealer>21)) { finalMessage.text = "Has ganado! Enhorabuena.";} 
        else { finalMessage.text = "Has periddo. Victoria para la banca."; }

        hitButton.interactable = false;
        stickButton.interactable = false;

    }


    public void PlayAgain()
    {
        hitButton.interactable = true;
        stickButton.interactable = true;
        finalMessage.text = "";
        player.GetComponent<CardHand>().Clear();
        dealer.GetComponent<CardHand>().Clear();          
        cardIndex = 0;
        ShuffleCards();
        StartGame();
    }
    
}


